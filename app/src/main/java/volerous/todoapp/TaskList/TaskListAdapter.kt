package volerous.todoapp.TaskList

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_task_list.view.*
import kotlinx.android.synthetic.main.task_list_item.view.*
import volerous.todoapp.Data.Objects.Task
import volerous.todoapp.R

class MyTaskRecyclerViewAdapter(private val mValues: ArrayList<Task>, private val mListener: TaskListFragment.OnFragmentInteractionListener?, private val mVM: TaskListViewModel) : RecyclerView.Adapter<MyTaskRecyclerViewAdapter.ViewHolder>() {
    private var mTasks = mValues.toMutableList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.task_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mTasks[position], mListener!!)
    }


    override fun getItemCount(): Int {
        return mTasks.size
    }
    fun getItem(position: Int): Task {
        return mTasks[position]
    }
    fun updateList(tasks: List<Task>) {
        mTasks = tasks.toMutableList()
        notifyDataSetChanged()
    }
    fun removeTask(pos: Int){
        mVM.markTask(mTasks[pos])
        mTasks.removeAt(pos)
        notifyItemRemoved(pos)
    }

    inner class ViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {
        var mTask: Task? = null


        fun bind(task: Task, listener: TaskListFragment.OnFragmentInteractionListener){
            mTask = task
            mView.task_title.text = task.title
            mView.setOnClickListener { _ -> listener.onFragmentInteraction(task) }
        }
        override fun toString(): String {
            return super.toString() + " '" + mTask!!.title + "'"
        }
    }
}