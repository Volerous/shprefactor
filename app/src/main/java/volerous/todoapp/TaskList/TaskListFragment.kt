package volerous.todoapp.TaskList

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_task_list.*
import volerous.todoapp.Data.Objects.Task

import volerous.todoapp.R


class TaskListFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private var taskAdapter: MyTaskRecyclerViewAdapter? = null
    private var tasksViewModel: TaskListViewModel? = null
    private var taskListAdapter: MyTaskRecyclerViewAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Get View Model and attach the task list to the fragment
        tasksViewModel = ViewModelProviders.of(this).get(TaskListViewModel::class.java)
        tasksViewModel?.getTasks()?.observe(this, Observer { tasks ->
            (reclist.adapter as MyTaskRecyclerViewAdapter).updateList(tasks!!)
        })
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // inflate base view
        val ret_view = inflater.inflate(R.layout.fragment_task_list, container, false)

        // Add on onclick listener for FAB
        ret_view.findViewById<FloatingActionButton>(R.id.fab).setOnClickListener{
            _ -> listener?.onFragmentInteraction(Task())
        }

        // Attach the Line SEperator and The Swipe handler
        val view = ret_view.findViewById<RecyclerView>(R.id.reclist)
        if (view is RecyclerView){
            val context = view.context
            view.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

            // Attach Swipe handler
            val swipeHandler = object: SwipeToDeleteTaskCallback(context){
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    (reclist.adapter as MyTaskRecyclerViewAdapter).removeTask(viewHolder.adapterPosition)
                }
            }
            val itemTouchHelper = ItemTouchHelper(swipeHandler)
            itemTouchHelper.attachToRecyclerView(view)
            taskAdapter = MyTaskRecyclerViewAdapter(ArrayList<Task>(),listener,tasksViewModel!!)
            view.adapter = taskAdapter
            view.setHasFixedSize(true)
        }
        return ret_view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onResume() {
        super.onResume()
//        taskListAdapter = MyTaskRecyclerViewAdapter(ArrayList<Task>(),listener,tasksViewModel!!)
//        reclist.adapter = taskListAdapter
        Log.d("frag test", this.view?.findViewById<RecyclerView>(R.id.reclist)?.adapter.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(task: Task)
    }

    companion object {
        fun newInstance() = TaskListFragment()
    }
}
