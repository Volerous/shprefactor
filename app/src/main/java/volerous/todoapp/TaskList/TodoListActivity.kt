package volerous.todoapp.TaskList

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_todo_list.*
import kotlinx.android.synthetic.main.app_bar_todo_list.*
import volerous.todoapp.AddEditTask.AddEditTaskFragment
import volerous.todoapp.Data.Objects.Task
import volerous.todoapp.R
import volerous.todoapp.R.id.*

class TodoListActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener
        , TaskListFragment.OnFragmentInteractionListener
        , AddEditTaskFragment.OnFragmentInteractionListener {
    val taskListFragment = TaskListFragment()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todo_list)
        setSupportActionBar(toolbar)
        if (savedInstanceState == null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.add(R.id.fragment_container,taskListFragment,"MainFrag")
            ft.commit()
        }
        Log.d("Fragment added Test", supportFragmentManager.fragments.toString())

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if (supportFragmentManager.backStackEntryCount >= 1){
                supportFragmentManager.popBackStack()
                supportFragmentManager.executePendingTransactions()
            } else {
                super.onBackPressed()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.todo_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onFragmentInteraction(task: Task) {
        val taskItemFragment = AddEditTaskFragment()
        val ft = supportFragmentManager.beginTransaction()
        ft.add(fragment_container, taskItemFragment, "AddEditFrag")
        ft.hide(supportFragmentManager.findFragmentByTag("MainFrag"))
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun onTaskEnterComplete() {
        val ft = supportFragmentManager.beginTransaction()
        ft.remove(supportFragmentManager.findFragmentByTag("AddEditFrag"))
        ft.show(supportFragmentManager.findFragmentByTag("MainFrag"))
        ft.commit()
    }
}
