package volerous.todoapp.TaskList

import android.content.Context
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import volerous.todoapp.R

/**
 * Created by root on 3/21/18.
 */
abstract class SwipeToDeleteTaskCallback(context: Context): ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
    private val background = ColorDrawable()
    private val backgroundColor = Color.parseColor("#f44336")
    private val deleteIcon = ContextCompat.getDrawable(context, R.drawable.ic_check_white_24dp)
    private val intrinsicWidth = deleteIcon.intrinsicWidth
    private val intrinsicHeight = deleteIcon.intrinsicHeight
    private val clearPaint = Paint().apply { xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR) }

    override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?): Boolean {
        return false
    }
    override fun getMovementFlags(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?): Int {
        /**
         * To disable "swipe" for specific item return 0 here.
         * For example:
         * if (viewHolder?.itemViewType == YourAdapter.SOME_TYPE) return 0
         * if (viewHolder?.adapterPosition == 0) return 0
         */
        return super.getMovementFlags(recyclerView, viewHolder)
    }

    override fun onChildDraw(c: Canvas?, recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        val taskView = viewHolder!!.itemView
        val taskViewHeight = taskView!!.height
        val isCanceled = dX == 0f && !isCurrentlyActive

        if (isCanceled) {
            clearCanvas(c, taskView.right + dX, taskView.top.toFloat(), taskView.right.toFloat(), taskView.bottom.toFloat())
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            return
        }
        val p = Paint()
        p.setARGB(255, 76,175, 70)
        if (dX > 0){
            c?.drawRect((taskView.left - 15.0).toFloat(), taskView.top.toFloat(), dX, taskView.bottom.toFloat(),p)
        } else {
            c?.drawRect(taskView.right.toFloat()+dX, taskView.top.toFloat(), taskView.right.toFloat(), taskView.bottom.toFloat(),p)
        }

        // Calculate position of delete icon
        val deleteIconTop = taskView.top + (taskViewHeight - intrinsicHeight) / 2
        val deleteIconMargin = (taskViewHeight - intrinsicHeight) / 2
        val deleteIconLeft = taskView.left
        val deleteIconRight = taskView.left + deleteIconMargin*2
        val deleteIconBottom = deleteIconTop + intrinsicHeight
        // Draw the delete icon

        deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom)
        deleteIcon.draw(c)

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }
    private fun clearCanvas(c: Canvas?, left: Float, top: Float, right: Float, bottom: Float) {
        c?.drawRect(left, top, right, bottom, clearPaint)
    }
}
