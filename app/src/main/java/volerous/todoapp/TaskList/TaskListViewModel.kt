package volerous.todoapp.TaskList

import volerous.todoapp.Data.Repo.TaskRepo
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import volerous.todoapp.Data.Objects.Task
import volerous.todoapp.di.TaskListApplication
import javax.inject.Inject

class TaskListViewModel: ViewModel(), LifecycleObserver{
    @Inject lateinit var taskRepo: TaskRepo
    private val compositeDisposable = CompositeDisposable()
    private var liveTaskData: LiveData<List<Task>>? = null

    init {
        initdagger()
    }
    fun getTasks(): LiveData<List<Task>> {
        if (liveTaskData == null) {
            liveTaskData = MutableLiveData<List<Task>>()
            liveTaskData = taskRepo.getTasksList()
        }
        return liveTaskData as LiveData<List<Task>>
    }
    fun markTask(task: Task) {
        task.completed = true
        taskRepo.updateTasks(listOf<Task>(task))
    }
    fun insertTask(task: Task){
        taskRepo.insertTasks(listOf(task))
    }

    private fun initdagger() = TaskListApplication.appComponent.inject(this)
}