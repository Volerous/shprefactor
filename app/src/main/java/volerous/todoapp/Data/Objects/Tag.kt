package volerous.todoapp.Data.Objects

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
@Entity(tableName = "tag")
data class Tag(
        @PrimaryKey @ColumnInfo(name ="title") var title: String="Test Tag",
        @ColumnInfo(name = "color") var color:String = "123456"
)