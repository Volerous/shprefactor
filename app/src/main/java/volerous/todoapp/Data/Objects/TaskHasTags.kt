package volerous.todoapp.Data.Objects

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey

@Entity(tableName = "task_has_tags", primaryKeys = ["task_id","tag"])
data class TaskWithTags(@ForeignKey(entity = Task::class, parentColumns = ["id"], childColumns = ["task_id"]) var task_id: Int,
                        @ForeignKey(entity = Tag::class, parentColumns = ["title"], childColumns = ["tag"])var tag: String)
{}