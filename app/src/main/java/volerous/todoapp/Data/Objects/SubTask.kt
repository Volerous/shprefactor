package volerous.todoapp.Data.Objects

import android.arch.persistence.room.*

@Entity(tableName = "subtask",foreignKeys = arrayOf(ForeignKey(entity = Task::class,
                                            parentColumns = arrayOf("id"),
                                            childColumns = arrayOf("parent_task_id"),
                                            onDelete = ForeignKey.CASCADE)))
data class SubTask(
        @PrimaryKey(autoGenerate = true) var id: Int = 0,
        @ColumnInfo(name = "title") var title: String = "Test SubTask",
        @ColumnInfo(name = "parent_task_id") var parent_task_id:Int = 0,
        @ColumnInfo(name = "completed") var completed:Boolean = false
)
//    @Ignore
//    constructor(title: String,completed: Boolean): this(0,title,0,completed)
//    constructor():this(0,"Test SubTask",0,false)
//
//}