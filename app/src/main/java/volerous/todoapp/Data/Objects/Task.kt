package volerous.todoapp.Data.Objects

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.util.*

@Entity(tableName = "task")
data class Task(
        @PrimaryKey(autoGenerate = true) var id: Int=0,
        @ColumnInfo(name = "title") var title:String="Test",
        @ColumnInfo(name = "priority") var priority: Int=3,
        @ColumnInfo(name = "due_date") var due_date:LocalDateTime? = null,
        @ColumnInfo(name = "description") var description: String = "Test Description",
        @ColumnInfo(name = "completed") var completed: Boolean= false,
        @Ignore var subtasks : MutableList<SubTask> = mutableListOf(SubTask()),
        @Ignore var tags: MutableList<Tag> = mutableListOf(Tag())
){
}