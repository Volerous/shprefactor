package volerous.todoapp.Data.Room

import volerous.todoapp.Data.Objects.TaskWithTags
import android.arch.persistence.room.*

@Dao
interface TaskHasTagsDAO{
    @Insert()
    fun insertTaskHasTags(item: TaskWithTags): Long

    @Delete
    fun deleteTaskHasTags(item: TaskWithTags)

    @Update
    fun updateTaskHasTags(item: TaskWithTags)
}