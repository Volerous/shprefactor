package volerous.todoapp.Data.Room

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.migration.Migration

class Migration_1_2 : Migration(1,2){
    override fun migrate(database: SupportSQLiteDatabase) {

        database.execSQL("CREATE TABLE `task_temp` (\n" +
                "\t`id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`title`\tTEXT NOT NULL,\n" +
                "\t`priority`\tINTEGER NOT NULL,\n" +
                "\t`description`\tTEXT NOT NULL,\n" +
                "\t`completed`\tINTEGER NOT NULL,\n" +
                "\tdue_date TEXT \n" +
                ");\n")
        database.execSQL("create table task_has_tags_temp(\n" +
                "task_id INTEGER not null,\n" +
                "tag TEXT NOT  NULL,\n" +
                "primary key (task_id,tag)\n" +
                ");")
        database.execSQL("create table `tag_temp`(\n" +
                "`title` STRING not null primary key,\n" +
                "`color` STRING not null\n" +
                ");\n")
        database.execSQL("insert into tag_temp(title, color) values(\"Test Tag\", \"123456\");\n")
        database.execSQL("insert into task_has_tags_temp(task_id,tag)\n" +
                "select a.task_id, b.title\n" +
                "from task_has_tags as a inner join tag as b where b.title is not null and b.title is not \"\";\n")



        database.execSQL("insert into task_temp(id, title, priority, description, completed)\n" +
                "select id, title, priority, description, completed\n" +
                "from task;\n")
        database.execSQL("drop table task;\n")
        database.execSQL("drop table task_has_tags;")
        database.execSQL("drop table tag;")
        database.execSQL("alter table tag_temp rename to `tags`;")
        database.execSQL("alter table task_has_tags_temp rename to task_has_tags;\n")
        database.execSQL("alter table task_temp rename to task;\n")
    }
}