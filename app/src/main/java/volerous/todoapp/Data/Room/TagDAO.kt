package volerous.todoapp.Data.Room

import volerous.todoapp.Data.Objects.Tag
import android.arch.persistence.room.*
import io.reactivex.Flowable

@Dao
interface TagDAO{
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertTag(tag: Tag): Long

    @Delete
    fun deletetags(vararg tag: Tag)

    @Update
    fun updatetags(vararg tag: Tag)

    @Query("SELECT * FROM tag")
    fun alltags(): Flowable<List<Tag>>
}