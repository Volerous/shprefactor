package volerous.todoapp.Data.Room

import volerous.todoapp.Data.Objects.SubTask
import volerous.todoapp.Data.Objects.Task
import android.arch.persistence.room.*
import io.reactivex.Flowable

@Dao
interface SubTaskDAO{
    @Insert
    fun insertSubTasks(subtasks: List<SubTask>): List<Long>

    @Delete
    fun deleteSubTasks(subtasks: List<SubTask>)

    @Update
    fun updateSubTasks(subtasks: List<SubTask>)

    @Query("SELECT * FROM subtask")
    fun allSubTasks(): Flowable<List<SubTask>>

    @Query("SELECT * FROM subtask WHERE subtask.parent_task_id =:task_id")
    fun getSubTasksForTasks(task_id: Int): Flowable<List<SubTask>>
}