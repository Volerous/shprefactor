package volerous.todoapp.Data.Room

import volerous.todoapp.Data.Objects.Task
import android.arch.persistence.room.*
import io.reactivex.Flowable
import volerous.todoapp.Data.Objects.Tag

@Dao
interface TaskDAO{
    @Insert
    fun insertTasks(task: Task): Long

    @Delete
    fun deleteTasks(tasks: List<Task>)

    @Update
    fun updateTasks(tasks: List<Task>)

    @Query("SELECT * FROM task WHERE task.completed = 0")
    fun allTasks(): Flowable<List<Task>>

    @Query("select a.* from tag as a inner join task_has_tags" +
            " as b on a.title = b.tag where b.task_id = :task_id")
    fun getTagsFromTask(task_id: Int): Flowable<List<Tag>>
}