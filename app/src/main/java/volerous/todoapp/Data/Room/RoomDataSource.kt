package volerous.todoapp.Data.Room

import android.arch.persistence.room.*
import volerous.todoapp.Data.Objects.SubTask
import volerous.todoapp.Data.Objects.Tag
import volerous.todoapp.Data.Objects.Task
import volerous.todoapp.Data.Objects.TaskWithTags
import android.content.Context
import volerous.todoapp.Data.Converters.Converter

@Database(entities = [Task::class, Tag::class, SubTask::class, TaskWithTags::class], version = 2, exportSchema = true)
@TypeConverters(Converter::class)
abstract class RoomDataSource: RoomDatabase(){
    abstract fun taskDAO(): TaskDAO
    abstract fun taskHasTagsDAO(): TaskHasTagsDAO
    abstract fun tagDAO(): TagDAO
    abstract fun subTaskDAO(): SubTaskDAO
    companion object {
         fun buildPersistentDatabase(context: Context): RoomDataSource = Room.databaseBuilder(
                 context.applicationContext,
                 RoomDataSource::class.java,
                 "main.db"
         ).fallbackToDestructiveMigration().build()
    }
}