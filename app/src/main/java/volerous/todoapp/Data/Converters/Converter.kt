package volerous.todoapp.Data.Converters

import android.arch.persistence.room.TypeConverter
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*

class Converter{
    @TypeConverter
    fun fromTimeStamp(stamp: String?): LocalDateTime? {
        if (stamp == null) {
            return null
        } else {
            val actual = LocalDateTime.parse(stamp, DateTimeFormatter.ISO_DATE_TIME)
            return actual
        }
    }

    @TypeConverter
    fun toTimeStamp(date: LocalDateTime?): String? {
        if (date == null){
            return null
        } else {
            val dateTime = LocalDateTime.of(date.toLocalDate(),date.toLocalTime())
            val actual  = dateTime.format(DateTimeFormatter.ISO_DATE_TIME)
            return actual
        }
    }
}