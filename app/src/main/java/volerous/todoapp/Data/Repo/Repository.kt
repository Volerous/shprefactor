package volerous.todoapp.Data.Repo

import volerous.todoapp.Data.Objects.Task
import android.arch.lifecycle.LiveData
import io.reactivex.Flowable

interface Repository{
    fun getAllTasks(): Flowable<List<Task>>
    fun getTasksList(): LiveData<List<Task>>
    fun insertTasks(tasks: List<Task>)
    fun updateTasks(tasks: List<Task>)
    fun deleteTasks(tasks: List<Task>)
}