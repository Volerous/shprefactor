package volerous.todoapp.Data.Repo

import volerous.todoapp.Data.Objects.Task
import volerous.todoapp.Data.Room.RoomDataSource
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import volerous.todoapp.Data.Objects.SubTask
import volerous.todoapp.Data.Objects.Tag
import volerous.todoapp.Data.Objects.TaskWithTags
import javax.inject.Inject

class TaskRepo @Inject constructor(
        private val mainDB: RoomDataSource
): Repository {
    val allCompositeDisposable: MutableList<Disposable> = arrayListOf()

    override fun getAllTasks() = mainDB.taskDAO().allTasks()

    override fun insertTasks(tasks: List<Task>) {
        doAsync {
            for (task in tasks){
                val task_id = mainDB.taskDAO().insertTasks(task)
                Log.d("task insert", task_id.toString())
                Log.d("Task insert", task.completed.toString())
                task.subtasks.forEach {
                    it.parent_task_id = task_id.toInt()
                }
                mainDB.subTaskDAO().insertSubTasks(task.subtasks)
                task.tags.forEach {
                    mainDB.tagDAO().insertTag(it)
                    mainDB.taskHasTagsDAO().insertTaskHasTags(TaskWithTags(task_id.toInt(),it.title))
                }
            }
        }
    }

    override fun getTasksList(): LiveData<List<Task>> {
        val roomTaskDAO = mainDB.taskDAO()
        val mutableLiveData = MutableLiveData<List<Task>>()
        val disposable = roomTaskDAO.allTasks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({tasks: List<Task> ->
                    tasks.forEach({it -> getTagsAndSubTasks(it)})
                    mutableLiveData.value = tasks
                }, {t: Throwable? -> t!!.printStackTrace() })
        allCompositeDisposable.add(disposable)
        return mutableLiveData
    }
    fun getTagsAndSubTasks(task: Task): Task{
        val subTaskDAO = mainDB.subTaskDAO()
        val disposable_1 = subTaskDAO.getSubTasksForTasks(task.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({tasks: List<SubTask> ->
                    task.subtasks = tasks.toMutableList()
                }, {t: Throwable? -> t!!.printStackTrace() })
        val disposable_2 = mainDB.taskDAO().getTagsFromTask(task.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({tags: List<Tag> ->
                    task.tags = tags.toMutableList()
                }, {t: Throwable -> t.printStackTrace() })
        allCompositeDisposable.add(disposable_1)
        allCompositeDisposable.add(disposable_2)
        return task
    }
    override fun deleteTasks(tasks: List<Task>) {
        doAsync {
            mainDB.taskDAO().deleteTasks(tasks)
        }
    }

    override fun updateTasks(tasks: List<Task>) {
        doAsync {
            mainDB.taskDAO().updateTasks(tasks)
        }
    }

}