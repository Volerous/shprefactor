package volerous.todoapp.di

import dagger.Component
import volerous.todoapp.TaskList.TaskListViewModel
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RoomDBModule::class))
@Singleton interface AppComponent{
    fun inject(TaskListViewModel: TaskListViewModel)
}