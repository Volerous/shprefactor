package volerous.todoapp.di

import android.app.Application

class TaskListApplication: Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }
    fun initDagger() {
        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .roomDBModule(RoomDBModule())
                .build()
    }
}