package volerous.todoapp.di

import android.arch.persistence.room.RoomDatabase
import android.content.Context
import dagger.Module
import dagger.Provides
import volerous.todoapp.Data.Room.RoomDataSource
import javax.inject.Singleton

@Module
class RoomDBModule {
    @Provides @Singleton fun provideRoomTaskDataSource(context: Context) =
            RoomDataSource.buildPersistentDatabase(context)
}