package volerous.todoapp.di

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val TaskListApplication: TaskListApplication) {
    @Provides @Singleton fun provideContext(): Context = TaskListApplication
}