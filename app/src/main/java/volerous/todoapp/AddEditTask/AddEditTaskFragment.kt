package volerous.todoapp.AddEditTask

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.Bindable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_add_edit_task.*
import volerous.todoapp.Data.Objects.Task
import volerous.todoapp.R
import volerous.todoapp.TaskList.TaskListViewModel
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class AddEditTaskFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null
    private var tasksViewModel: TaskListViewModel? = null
    private var task: Task = Task()
    private var dateformater = DateTimeFormatter.ofPattern(" dd-MMM-yy")
    private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tasksViewModel = ViewModelProviders.of(this).get(TaskListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val retview =inflater.inflate(R.layout.fragment_add_edit_task, container, false)
        val datepicker = retview.findViewById<EditText>(R.id.date)
        if (task.due_date == null ) {
            task.due_date = LocalDateTime.now()
        }

        datepicker.setText(task.due_date?.format(dateformater), TextView.BufferType.EDITABLE)
        val datepickerlistener = DatePickerDialog.OnDateSetListener{
            view, year, month, day ->
            task.due_date = task.due_date?.withYear(year)
            task.due_date = task.due_date?.withMonth(month+1)
            task.due_date = task.due_date?.withDayOfMonth(day)

            datepicker.setText(task.due_date?.format(dateformater), TextView.BufferType.EDITABLE)
        }
        datepicker.setOnClickListener{
            val datePickerDialog = DatePickerDialog(this.context,datepickerlistener,task.due_date!!.year,task.due_date!!.monthValue-1,task.due_date!!.dayOfMonth)
            datePickerDialog.datePicker.firstDayOfWeek = 2
            datePickerDialog.show()
        }
        val timePicker = retview.findViewById<EditText>(R.id.time)
        timePicker.setText(task.due_date!!.format(timeFormatter), TextView.BufferType.EDITABLE)
        val timePickerListener = TimePickerDialog.OnTimeSetListener{
            view, hour,minute ->
            task.due_date = task.due_date?.withHour(hour)
            task.due_date = task.due_date?.withMinute(minute)
            timePicker.setText(task.due_date!!.format(timeFormatter), TextView.BufferType.EDITABLE)
        }
        timePicker.setOnClickListener{
            val timePickerDialog = TimePickerDialog(this.context, timePickerListener,task.due_date!!.hour,task.due_date!!.minute, true)
            timePickerDialog.show()
        }
        val doneBtn = retview.findViewById<Button>(R.id.doneBtn)
        doneBtn.setOnClickListener{
            _ ->
            task.title = title.text.toString()
            task.description = description.text.toString()
            tasksViewModel?.insertTask(task)
            listener?.onTaskEnterComplete()
        }
        return retview
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        fun onTaskEnterComplete()
    }
    companion object {
        fun newInstance() = AddEditTaskFragment()
    }
}
